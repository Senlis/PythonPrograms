import RPi.GPIO as GPIO # allows us to control pi pins
from ping3 import ping # allows us to check on NAS

# setup
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(18, GPIO.OUT) # green light (NAS)
GPIO.setup(23, GPIO.OUT) # red light (NAS)
GPIO.setup(4, GPIO.OUT) # red light (internet)
GPIO.setup(17, GPIO.OUT) # blue light (internet)
# ping the NAS
ping_result = ping('192.168.1.145')

if ping_result != None: # if we get a response (green light on)
    GPIO.output(18, GPIO.HIGH)
    GPIO.output(23, GPIO.LOW)
else: # if we don't get a response (red light on)
    GPIO.output(18, GPIO.LOW)
    GPIO.output(23, GPIO.HIGH)
    
# ping Google
ping_result = ping('google.com')

if ping_result != None: # if we get a response (blue light on)
    GPIO.output(17, GPIO.HIGH)
    GPIO.output(4, GPIO.LOW)
else: # if we don't get a response (red light on)
    GPIO.output(17, GPIO.LOW)
    GPIO.output(4, GPIO.HIGH)    
        
        

