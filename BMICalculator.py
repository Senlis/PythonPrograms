'''
Program receives weight and height information from user and calculates BMI
'''

from tkinter import Tk, Frame, Label, Button, Entry, StringVar, OptionMenu, RIDGE

class Calculator:
	'''
	Class manages BMI calculatios
	'''

class GUI:
	'''
	Class to create and manage user interface
	'''
	def __init__(self):
		# set up window
		root = Tk()
		root.title("BMI Calculator")

		# define all GUI objects
		# Top Frame
		top_frame = Frame(root, borderwidth = 10, relief = RIDGE) # upper left frame.  Contains text boxes for input
		bottom_frame = Frame(root, borderwidth = 10, relief = RIDGE) # lower left frame.  Displays BMI
		right_frame = Frame(root, borderwidth = 10, relief = RIDGE) # right frame.  Reports overweight, normal, underweight information

		height_label = Label(top_frame, text = "Height")
		weight_label = Label(top_frame, text = "Weight")

		# drop down menus
		self.height_control = StringVar(top_frame)
		self.height_control.set("inches")
		self.height_drop_down = OptionMenu(top_frame, self.height_control, "inches", "meters")

		self.weight_control = StringVar(top_frame)
		self.weight_control.set("lbs")
		weight_drop_down = OptionMenu(top_frame, self.weight_control, "lbs", "kg")

		self.height_entry = Entry(top_frame, width = 10)
		self.weight_entry = Entry(top_frame, width = 10)

		# Bottom frame
		answer_title_label = Label(bottom_frame, text = "Your BMI is: ")
		self.answer_data_label = Label(bottom_frame, text = "####")

		# Right Frame
		self.obese_label = Label(right_frame, text = "You are obese (over 30)")
		self.overweight_label = Label(right_frame, text = "You are overweight (25-29.9)")
		self.normal_label = Label(right_frame, text = "You are a normal weight (18.5-24.9)")
		self.underweight_label = Label(right_frame, text = "You are underweight (Below 18.5)")

		calculate_button = Button(top_frame, text = "Calculate", command = self.calculate_BMI)

		# pack everything
		# Top frame
		height_label.grid(row = 0, column = 0)
		weight_label.grid(row = 1, column = 0)
		self.height_entry.grid(row = 0, column = 1)
		self.weight_entry.grid(row = 1, column = 1)
		self.height_drop_down.grid(row = 0, column = 2)
		weight_drop_down.grid(row = 1, column = 2)
		calculate_button.grid(row = 2, column = 0, columnspan = 3)

		# Bottom frame
		answer_title_label.grid(row = 0, column = 0)
		self.answer_data_label.grid(row = 0, column = 1)

		# Right frame
		self.obese_label.pack()
		self.overweight_label.pack()
		self.normal_label.pack()
		self.underweight_label.pack()

		# Pack frames
		top_frame.grid(row = 0, column = 0)
		bottom_frame.grid(row  = 1, column = 0, sticky = 'we')
		right_frame.grid(row = 0, column = 1, rowspan = 2, sticky = 'ns')

		root.mainloop()

	def calculate_BMI(self):
		'''
		Calculates BMI and display amount in bottom frame.  Also highlights results in right frame
		'''

		# reset summary here and answer label text color
		self.obese_label.config(fg="black", borderwidth=0)
		self.overweight_label.config(fg="black", borderwidth=0)
		self.normal_label.config(fg="black", borderwidth=0)
		self.underweight_label.config(fg="black", borderwidth=0)
		self.answer_data_label.config(fg="black")

		# retrieve values from entry fields
		try:
			height_input = float(self.height_entry.get())
			weight_input = float(self.weight_entry.get())
		except ValueError as e:
			self.answer_data_label.config(fg="red")
			self.answer_data_label['text'] = "Error"
			return
			
		height_in_meters = 0.0
		weight_in_kg = 0.0
		
		if (self.weight_control.get() == "lbs"): # convert mass to kg here
			# 1 lbs = 0.45359237 kg
			weight_in_kg = weight_input * 0.45359237
		else:
			weight_in_kg = weight_input

		if (self.height_control.get() == "inches"): # convert height to meters here
			# 1 in = 0.0254 m
			height_in_meters = height_input * 0.0254
		else:
			height_in_meters = height_input


		# calculate BMI
		BMI = weight_in_kg / (height_in_meters * height_in_meters)

		# display BMI number
		self.answer_data_label['text'] = round(BMI, 2)

		# l1 = Label(root, text="This", borderwidth=2, relief="groove")
		if BMI >= 30.0:
			self.obese_label.config(fg="red", borderwidth = 1, relief = "solid")
		elif BMI >= 25:
			self.overweight_label.config(fg="#d6ab17", borderwidth=1, relief="solid")
		elif BMI >= 18.5:
			self.normal_label.config(fg="green", borderwidth=1, relief="solid")
		else:
			self.underweight_label.config(fg="#d6ab17", borderwidth=1, relief="solid")

gui = GUI()
