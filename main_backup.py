import os
import subprocess
import datetime


class Logger:
    '''
    class is intended to be instantiated once.  It enforces the log file being a constant and
    accessable globally
    '''

    def __init__(self, backup_file):
        '''
        captures backup_file path and initiates log
        '''
        self.__file = backup_file
        self.__begin_log()

    def get_file(self):
        '''
        single method of accessing log file directly
        '''
        return self.__file

    def __begin_log(self):
        '''
        Function clears log file and writes first entries
        '''
        os.system('echo "Begin log file" > {0} 2>&1'.format(self.__file))
        self.write("Current time: {0}".format(datetime.datetime.now()))
        self.write("******************************************")

    def write(self, message):
        '''
        Function consolidates entry into the logfile
        @param String message : message to be written
        '''
        os.system('echo "{0}" >> {1} 2>&1'.format(message, self.__file))


shared_log = Logger("/root/backup_log.txt")  # global log file


class BackupOrder:
    '''
    container to hold backup parameters
    '''
    def __init__(self, drive_name, drive_UUID, source_directory,
                 destination_directory, excludes):
        self.drive_name = drive_name  # this name is for logging
        self.drive_UUID = drive_UUID
        self.source_directory = source_directory
        self.destination_directory = destination_directory  # relative location
        self.excludes = excludes


def prepare_for_backup():
    '''
    function setups up script variables and then calls the
        main backup script "begin backup"
    '''
    # first set up drives
    first_storage_drive_UUID = "39229d46-f7e1-4a59-b106-84e61640d02a"
    first_multimedia_drive_UUID = "162e9d8f-7a71-4325-9636-b886690187e8"

    # second set of drives, since it has larger capacity, it is only one drive to
    # accomplish what the two drives above do
    new_drive_UUID = "06418f2a-d50a-46a1-afd8-56885cdcc94c"

    # ============ pepare parameters for all backups ===============
    # create dummy backup order

    backup_orders = []  # where we collect all of our backups

    if drive_is_connected(first_storage_drive_UUID):
        shared_log.write("== First storage drive was found == ")

        first_drive_backup = BackupOrder(
                                        drive_name="Storage Drive",
                                        drive_UUID=first_storage_drive_UUID,
                                        source_directory="/Storage/Storage/", 
                                        destination_directory="/",  # backup to the mount point 
                                        excludes="--exclude 'Movies' --exclude 'TV Shows'")

        backup_orders.append(first_drive_backup)
    else:
        shared_log.write("== First storage drive was __not__ found ==")

    if drive_is_connected(first_multimedia_drive_UUID):
        shared_log.write("== First multimedia drive was found == ")

        movies_backup = BackupOrder(
                                drive_name="Multimedia Movies",
                                drive_UUID=first_multimedia_drive_UUID,
                                source_directory="/Storage/Storage/Movies/", 
                                destination_directory="Movies/", 
                                excludes="")

        backup_orders.append(movies_backup)

        TV_backup = BackupOrder(
                                drive_name="Multimedia TV Shows",
                                drive_UUID=first_multimedia_drive_UUID,
                                source_directory="/Storage/Storage/TV\ Shows/", 
                                destination_directory="TV\ Shows/",
                                excludes="")

        backup_orders.append(TV_backup)
    else:
        shared_log.write("== First Multimedia drive was __not__ found ==")

    if drive_is_connected(new_drive_UUID):
        shared_log.write("== New drive was found ==")

        new_drive_backup = BackupOrder(
                                drive_name="New Drive",
                                drive_UUID=new_drive_UUID,
                                source_directory="/Storage/Storage/", 
                                destination_directory="/",
                                excludes="")

        backup_orders.append(new_drive_backup)
    else:
        shared_log.write("== New drive was not found ==")

    # print all collected orders to log
    shared_log.write("Collected orders")
    for backup_order in backup_orders:
        shared_log.write(backup_order.drive_name)

    # run through each backup order
    for backup_order in backup_orders:
        begin_backup(order=backup_order)

    # end of script
    shared_log.write("******************************************")
    shared_log.write("* Backup script complete")


def drive_is_connected(drive_UUID):
    '''
    Checks if drive is connected.
    @param String drive_UUID : the UUID of the drive to be checked
    returns True is connected, False if not
    '''
    try:
        subprocess.check_output('lsblk -f | grep {0} >> {1} 2>&1'.format(drive_UUID, shared_log.get_file()), shell=True)
        return True  # Drive was found
    except subprocess.CalledProcessError:  # Drive was not found
        return False


def begin_backup(order):
    '''
    function checks if drive is mounted and then mounts it if necessary
    @param BackupOrder order: contains the details of the backup
    '''
    shared_log.write("{0}: Beggining backup routine".format(order.drive_name))

    backup_mount_point = "/NASBackup"  # todo: make the shared mountpoint global

    # Ensure our mount driectory exists
    ensure_directory_exists(backup_mount_point)

    # unmount whatever is in the backup directory is unmounted
    shared_log.write("Attempting pre-emtive dismount")
    try: 
        subprocess.check_output("umount -l {0} >> {1} 2>&1".format(backup_mount_point, shared_log.get_file()),
                                shell=True)
        shared_log.write("A drive was dismounted")
    except subprocess.CalledProcessError:  # the case that nothing was mounted
        shared_log.write("No drive was present")

    shared_log.write("Attempting drive mount")
    # mount the drive to back up to
    try:
        subprocess.check_output('mount UUID="{0}" {1} >> {2} 2>&1'.format(
            order.drive_UUID, backup_mount_point, shared_log.get_file()), shell=True)
        shared_log.write("Mount process successful")
    except subprocess.CalledProcessError:  # we were unable to mount
        shared_log.write("ERROR: begin_backup method unable to mount drive")
        return

    backup_to_directory = backup_mount_point + "/" + order.destination_directory

    # check that backup directory ensure_directory_exists
    ensure_directory_exists(backup_to_directory)

    # begin primary backup
    shared_log.write("******************************************")
    shared_log.write('{0}:Beggining backup'.format(order.drive_name))
    os.system("rsync -r {0} --delete {1} {2} >> {3} 2>&1".format(
        order.excludes, order.source_directory, backup_to_directory, shared_log.get_file()))
    shared_log.write('{0}:Main backup completed'.format(order.drive_name))

    # diff the directories that were just backed up
    shared_log.write("******************************************")
    shared_log.write('{0}:Diff command beggining'.format(order.drive_name))
    os.system('diff -r {0} {1} >> {2} 2>&1'.format(
        order.source_directory, backup_to_directory, shared_log.get_file()))
    shared_log.write('{0}:Diff commands completed'.format(order.drive_name))

    # unmount the drive
    try:
        subprocess.check_output("umount -l {0} >> {1} 2>&1".format(backup_mount_point, shared_log.get_file()),
                                shell=True)
    except subprocess.CalledProcessError:  #  unable to umount
        shared_log.write("{0}: unable to unmount drive".format(order.drive_name))

def drive_is_mounted(directory):
    '''
    function checks if drive is mounted
    @param directory: directory to check if mounted
    @returns boolean: True if mounted, False if not mounted
    '''

    try:  # check if destination directory has an external drive mounted
        subprocess.check_output('mount | grep {0} >> {1} 2>&1'.format(directory, shared_log.get_file()), shell=True)
    except subprocess.CalledProcessError:  # Drive was not mounted
        return False
    else:  # Drive is mounted
        return True


def ensure_directory_exists(backup_directory):
    '''
    function checks if backup_directory exists.  If not, function will create directory and
    any parent directories.  Function assumes script is running as root, and therefore will
    not run into any permission errors
    @param backup_directory: Directory to check.  Must be absolute path
    '''
    try:
        subprocess.check_output('cd {0} >> {1} 2>&1'.format(backup_directory, shared_log.get_file()), shell=True)
    except subprocess.CalledProcessError:
        # case that directory does not exist, create the directory and any subdirectories
        os.system('mkdir -p {0} >> {1} 2>&1'.format(backup_directory, shared_log.get_file()))
    else:  # directory does exist
        return


# beggining of main block of code
prepare_for_backup()
